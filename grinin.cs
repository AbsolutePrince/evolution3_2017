﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceDB
{
    [Serializable]
    //First commitmaster
    public class Card
    {
        public Header Header { get; set; }
        public List<Component> Components{ get; set; }
        public string Description
        {
            get { return Header.Number.ToString() + ". " + Header.Orderer.ToString() + " " + Header.Name.ToString(); }
        }

        public Card()

    //2 commitmaster
        {
            Header = new Header();
            Components = new List<Component>();
        }

        public override string ToString()
        {
            return Description;
        }
    }
}
