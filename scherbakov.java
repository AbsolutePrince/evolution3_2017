package seabattle.model.events;

import java.util.EventObject;

/*
 * Событие, связанное с результатом хода
 */
public class ResultOfShotEvent extends EventObject {
    
    public ResultOfShotEvent(Object source) { 
        super(source); 
    } 
}
