#include <iostream>
#include <conio.h>

#include "arg_check.h"
#include "file_io.h"

#define ERROR_WRONG_ARGS 1
#define ERROR_CANT_READ_INPUT 2

using namespace std;

int main(int argc, char * argv[])
{
  char * text = 0;

  if (!checkArgumentCorrectness(argc, argv))
  {
    printf("Wrong command line arguments.\n");
    return ERROR_WRONG_ARGS;
  }

  if (!loadTextFromFile("file.txt",text))
  {
    printf("Can't read file.txt");
    return ERROR_CANT_READ_INPUT;
  }

  // Освобождаем память
  free(text);

  return 0;
}